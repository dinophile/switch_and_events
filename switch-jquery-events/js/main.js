$(document).ready(function(){
	$('.switch').click(function(){
		$('.on').toggleClass('off');

		$('.status').html($('.status').text() == 'It\'s so bright in here!' ? 'Hey! Who turned off the lights?' : 'It\'s so bright in here!');

		$('body').toggleClass('dark');
	});
});
	
