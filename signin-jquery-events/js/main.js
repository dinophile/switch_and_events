$(document).ready(function(){
	$('.signin').click(function(){
		$('.modal').fadeIn('slow');
	});
	$('.close').click(function(){
		$('.modal').fadeOut('slow');
	});
	$('.submit').click(function(){
	 $(':input').addClass('error');
	});
	$(document).on("mouseover", "input.error", function(){
		$(this).removeClass('error');
	});
	$('.getstarted').on('click', function (e) {
			if ( e.target != this) return;
			$('.modal').fadeOut('slow');
	});
});
